'use client'
//* секция Библиотеки c функциями
import * as React from "react";
import dayjs from "dayjs";
import _ from 'lodash'
import { useHasMounted } from '@react-hooks-library/core'
//* endof  Библиотеки c функциями

//* секция Наши хелперы

import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store

//* endof  Контекст и store

//* секция Компоненты из библиотек
import Head from 'next/head'
import Image from 'next/image';

//* endof  Компоненты из библиотек

//* секция Наши компоненты
import AnimatedButton from '@components/animated/Button'
//* endof  Наши компоненты

export default function Component({ }) {
    //* библиотеки и неизменяемые значения
    //* endof библиотеки и неизменяемые значения


    //* контекст

    //* endof контекст

    //* состояние
    const [isHiddenByUser, setHiddenByUser] = React.useState(true);
    const [scrolledToShow, setScrolledToShow] = React.useState(false);
    const hasMounted = useHasMounted();
    //* endof состояние

    //* эффекты
    React.useEffect(() => {
        if (hasMounted) {
            setHiddenByUser(window.localStorage.getItem('is_bottom_banner_hidden_by_user') == 'true');
            console.log('setHiddenByUser', window.localStorage.getItem('is_bottom_banner_hidden_by_user') == 'true')
        }
    }, [hasMounted])

    React.useEffect(() => {
        if (typeof window !== 'undefined') {
            window.addEventListener('scroll', controlNavbar);

            return () => {
                window.removeEventListener('scroll', controlNavbar);
            };
        }
    }, []);

    //* endof эффекты

    //* функции-хелперы, НЕ ОБРАБОТЧИКИ
    // без useMemo, функция при каждом рендере создается новая. И поэтому window.removeEventListener не всегда работает как надо.
    let controlNavbar = React.useMemo(() => function controlNavbar() {
        if (typeof window !== 'undefined') {
            // console.log('window.scrollY', window.scrollY)
            if (window.scrollY > 150) { // 150px - нижняя граница верхнего баннера по отношению к началу окна.
                setScrolledToShow(true);
            }
        }
    }, []);

    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* обработчики
    function hideForever() {
        setHiddenByUser(true);
        window.localStorage.setItem('is_bottom_banner_hidden_by_user', 'true');
    }
    //* endof обработчики

    return !isHiddenByUser ? <>
        <div className={`desktop-block ${scrolledToShow ? 'shown-on-scroll' : 'hidden-until-scroll'} d-none d-md-flex bottom-banner bg-brand d-flex align-items-center justify-content-end border-radius-15`} >
            <AnimatedButton scalingMultiplier={5} variant="none" className="close-button ml-05 p-0 weight-200 d-flex all-center" onTap={hideForever}>
                <span className="d-inline-block color-gray2 size-38 weight-300 h-2 lh-2" style={{ transform: 'rotate(45deg)' }}>+</span>
            </AnimatedButton>

            <Image
                className="bg-image"
                src="/assets/img/banners/bottom-banner.png"
                alt=""
                width={261}
                height={250}
            />

            <div className="text-block mt-10 mr-35 d-flex flex-column align-items-center">
                <div className="color-white size-46">
                    Black Friday
                </div>

                <div className="size-40 mt-25 gradient-text-brand2 discount-text">
                    10%OFF
                </div>

                <div className="color-white size-16 color-gray2 mt-20">
                    Use code <span className="color-brand2">10FRIDAY</span> at checkout
                </div>

                <AnimatedButton variant="dark" className="buy-button mt-40 px-25 py-20 border-radius-15 weight-500">
                    <div className="text-wrapper">Shop now through Monday</div>
                </AnimatedButton>
            </div>
        </div>

        <div className={`w-100 mobile-block ${scrolledToShow ? 'shown-on-scroll' : 'hidden-until-scroll'} d-flex d-md-none bottom-banner bg-brand d-flex align-items-center justify-content-center`} >
            <AnimatedButton scalingMultiplier={5} variant="none" className="close-button ml-05 p-0 weight-200 d-flex all-center" onTap={hideForever}>
                <span className="d-inline-block color-gray2 size-38 weight-300 h-2 lh-2" style={{ transform: 'rotate(45deg)' }}>+</span>
            </AnimatedButton>

            <Image
                className="bg-image"
                src="/assets/img/banners/bottom-banner.png"
                alt=""
                width={261}
                height={250}
            />

            <div className="text-block mt-10 w-100 d-flex flex-column align-items-center">
                <div className="color-white size-46">
                    Black Friday
                </div>

                <div className="size-40 mt-25 gradient-text-brand2 discount-text">
                    10%OFF
                </div>

                <div className="color-white size-16 color-gray2 mt-20">
                    Use code <span className="color-brand2">10FRIDAY</span> at checkout
                </div>

                <AnimatedButton variant="dark" className="buy-button mt-40 px-25 py-15 border-radius-15 weight-500">
                    <div className="text-wrapper">Shop now</div>
                </AnimatedButton>
            </div>
        </div>
    </> : null
}