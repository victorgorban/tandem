//* секция Библиотеки c функциями
import * as React from "react";
import dayjs from "dayjs";
import _ from 'lodash'
//* endof  Библиотеки c функциями

//* секция Наши хелперы

import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store

//* endof  Контекст и store

//* секция Компоненты из библиотек
import Head from 'next/head'
import Image from 'next/image';

//* endof  Компоненты из библиотек

//* секция Наши компоненты
import AnimatedButton from '@components/animated/Button'
//* endof  Наши компоненты

export default function Component() {
    //* библиотеки и неизменяемые значения
    //* endof библиотеки и неизменяемые значения


    //* контекст

    //* endof контекст

    //* состояние
    const [isVisible, setVisible] = React.useState(true);
    //* endof состояние

    //* эффекты

    //* endof эффекты

    //* функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* обработчики

    //* endof обработчики

    return isVisible ? <>
        <div className="w-100 desktop-block position-relative d-none d-md-flex top-banner bg-brand d-flex all-center px-25 py-08" style={{ height: 54, overflowY: 'hidden' }}>
            <Image
                className="bg-image"
                src="/assets/img/banners/top-banner.png"
                alt=""
                width={212}
                height={54}
            />


            <div className="text-block size-16 d-flex all-center">
                <span className="color-white"><span className="weight-500">Black Friday</span><span className="d-none d-xl-inline">, 24-27 Nov</span></span>
                <div className="text-dot-divider mx-10"></div>
                <span className="color-brand2 weight-600 discount-text">
                    10%OFF
                </span>
                <div className="text-dot-divider mx-10"></div>
                <span className="color-white">
                    Use code <span className="weight-600 color-brand2">10FRIDAY</span><span className="d-none d-xl-inline"> at checkout</span>
                </span>
            </div>

            <div className="d-flex right-buttons">
                <AnimatedButton variant="default" className="buy-button px-15 py-10 border-radius-100 weight-500">
                    <div className="text-wrapper">Shop now</div>
                </AnimatedButton>
                <AnimatedButton scalingMultiplier={5} variant="none" className="ml-05 weight-200 d-none d-xl-flex all-center" onTap={() => setVisible(false)}>
                    <span className="d-inline-block color-gray2 size-38 weight-300 h-2 lh-2" style={{ transform: 'rotate(45deg)' }}>+</span>
                </AnimatedButton>
            </div>
        </div>

        <div className="bg-brand overflow-hidden">
            <AnimatedButton variant="none" scalingMultiplier={0.2} className="w-100 mobile-block d-flex d-md-none top-banner bg-brand d-flex align-items-center justify-content-end px-25 py-08" style={{ height: 54, overflowY: 'hidden' }}>
                <Image
                    className="bg-image"
                    src="/assets/img/banners/top-banner.png"
                    alt=""
                    width={212}
                    height={54}
                />

                <div className="text-block size-16 d-flex align-items-center">
                    <div className="color-white weight-500">
                        Black Friday, <span className="weight-600 color-brand2 discount-text">10%OFF</span>
                    </div>

                    <Image
                        className="ml-15"
                        src="/assets/img/icons/arrow-right-gray.svg"
                        alt=""
                        width={30}
                        height={30}
                    />
                </div>
            </AnimatedButton>
        </div>
    </> : null
}