//* секция Библиотеки c функциями
import * as React from 'react';
import NextTopLoader from 'nextjs-toploader';
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
import { ToastContainer, Slide as SlideTransition } from 'react-toastify';

//* endof  Компоненты из библиотек

//* секция Наши компоненты
import AnimatedPage from '@components/animated/Page'
import InitStores from '@components/_global/InitStores'
import PageTop from '@components/_global/pageTop'
import PageBottom from '@components/_global/pageBottom'
//* endof  Наши компоненты

//* секция Стили
import localFont from 'next/font/local'

const Graphik = localFont({
  src: [
    {
      path: './fonts/Graphik_LCG/GraphikLCG-Light.woff2',
      weight: '300',
      style: 'normal'
    },
    {
      path: './fonts/Graphik_LCG/GraphikLCG-Regular.woff2',
      weight: '400',
      style: 'normal'
    },
    {
      path: './fonts/Graphik_LCG/GraphikLCG-Medium.woff2',
      weight: '500',
      style: 'normal'
    },
    {
      path: './fonts/Graphik_LCG/GraphikLCG-Semibold.woff2',
      weight: '600',
      style: 'normal'
    },
    {
      path: './fonts/Graphik_LCG/GraphikLCG-Bold.woff2',
      weight: '700',
      style: 'normal'
    },
  ],
  variable: '--font-graphik_LCG'
})

// стили через import триггерят обновление fast-refresh, а то что в Head - нет. 
// К тому стили через import в продакшене минифицируются и объединяются в один файл.
import 'tippy.js/dist/tippy.css'; // стили из либ должны перезаписываться нашими. Поэтому либы ставить перед index.scss, а не после.
import 'react-date-range/dist/styles.css'; // main css file
// import 'react-date-range/dist/theme/default.css'; // theme css file

import 'font-awesome/css/font-awesome.min.css';
import 'react-toastify/dist/ReactToastify.css';

import '@assets/fonts/Graphik_LCG/stylesheet.css'
import '@assets/scss/index.scss'

//* endof Стили

export const metadata = {
  title: 'Сайт с тестовыми заданиями'
}

export default function Layout({ children: page }) {

  return (
    <html id="pageRoot">
      <body className={`${Graphik.variable}`}>
        <NextTopLoader />
        <InitStores />
        <PageTop />
        <main style={{minHeight: '100vh'}}>
          {page}
        </main>
        <PageBottom />
        {/* <Footer /> */}

        <ToastContainer
          position="top-right"
          autoClose={5000}
          theme="colored"
          transition={SlideTransition}
          newestOnTop={true}
          closeOnClick={true}
          closeButton={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </body>
    </html>
  )
}
