'use client'

//* секция Библиотеки c функциями
import * as React from "react";
import _ from 'lodash'
import { useRouter, usePathname } from 'next/navigation'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
import Link from 'next/link'
//* endof  Компоненты из библиотек

//* секция Виджеты
//* endof  Виджеты

//* секция Наши компоненты
//* endof  Наши компоненты


export default function Component({ }) {
    //* секция глобальное состояние из context
    //* endof глобальное состояние из context

    //* секция состояние
    // const router = useRouter();
    // const pathname = usePathname();

    //* endof состояние


    //* секция вычисляемые переменные, изменение состояния
    //* endof вычисляемые переменные, изменение состояния

    //* секция эффекты

    //* endof эффекты

    //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ
    
    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* секция обработчики

    //* endof обработчики

    return (
        <div className="copyright-line w-100 h-5 bg-brand">
            <div className="container w-100 h-100 d-flex all-center">
                <div className="copyright transform-uppercase size-12 color-gray5">
                    Разработано: 2024, Виктор Горбань
                </div>
            </div>
        </div>
    )
}
