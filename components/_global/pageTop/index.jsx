'use client'

//* секция Библиотеки c функциями
import * as React from "react";
import _ from 'lodash'
import { useRouter, usePathname } from 'next/navigation'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
import Link from 'next/link'
//* endof  Компоненты из библиотек

//* секция Виджеты
//* endof  Виджеты

//* секция Наши компоненты
import Button from '@components/animated/Button'
//* endof  Наши компоненты


export default function Component({ }) {
    //* секция глобальное состояние из context
    //* endof глобальное состояние из context

    //* секция состояние
    const pathname = usePathname();
    //* endof состояние

    //* секция вычисляемые переменные, изменение состояния
    
    //* endof вычисляемые переменные, изменение состояния

    //* секция эффекты

    //* endof эффекты

    //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* секция обработчики

    //* endof обработчики

    return (
        <>
            <div className="top-line w-100 h-5 bg-brand">
                <div className="container w-100 h-100 d-flex justify-content-between align-items-center">
                    <div className="widget-1 main-contact color-gray5">
                        <Link className="link style-none" href="https://t.me/victorgorban2"><i className="social-icon fa fa-telegram mr-05"></i> @victorgorban2</Link>
                    </div>

                    <div className="widget-2 social-links">
                        <Link className="link style-none" href="https://t.me/victorgorban2"><i className="social-icon fa fa-telegram mr-10"></i></Link>
                        <Link className="link style-none" href="https://stackoverflow.com/users/9047572/victor-gorban"><i className="social-icon fa fa-stackoverflow fa-code mr-05"></i></Link>
                    </div>
                </div>
            </div>

        </>
    )
}
