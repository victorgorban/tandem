"use client";
//* секция Библиотеки c функциями
import React from "react";
import AsyncSelect from "react-select/async";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты
export type ComponentProps = {
  /** Значение */
  value?: any;
  /** Колбек для открытия или закрытия меню */
  onMenuOpenOrClosed?: (isOpen: boolean) => void;
  /** Другие свойства html-элемента */
  [key: string]: any;
};

/**
 * Компонент для форм
 * @returns TSX Компонент
 */
export default function Component({
  value,
  onMenuOpenOrClosed = null,
  ...otherProps
}: ComponentProps): React.JSX.Element {
  //* секция глобальное состояние из context
  //* endof глобальное состояние из context

  //* секция состояние

  //* endof состояние

  //* секция вычисляемые переменные, изменение состояния

  //* endof вычисляемые переменные, изменение состояния

  //* секция эффекты

  //* endof эффекты

  //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ

  //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ

  //* секция обработчики
  function handleMenuOpen() {
    onMenuOpenOrClosed?.(true);
  }

  function handleMenuClose() {
    onMenuOpenOrClosed?.(false);
  }
  //* endof обработчики

  return (
    // TODO необходимо исправить отображение. Даже в обычной странице есть проблемы, не говоря об модалках. Может поставтить z-index для родителя... Ну и возможно применение transform отменяет z-index.

    <AsyncSelect
      noOptionsMessage={() => "Ничего не найдено..."}
      className="w-100"
      classNamePrefix="react-select"
      styles={{
        menu: (base) => ({ ...base, zIndex: 999 }),
        // menu: base => ({ ...base, zIndex: 9999 }),
      }}
      // menuPortalTarget={document.body}
      // document.body в модалке плохо работает. Можно не указывать, и поставить menuPosition="fixed", так работает
      menuShouldScrollIntoView={true}
      // menuPlacement="auto"
      // menuPosition={"fixed"}
      placeholder="Выбрать..."
      onMenuOpen={handleMenuOpen}
      onMenuClose={handleMenuClose}
      value={value}
      {...otherProps}
    />
  );
}
