"use client";
//* секция Библиотеки c функциями
import React from "react";
import _ from "lodash";
import { motion, AnimatePresence } from "framer-motion";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
import Link from "next/link";
//* endof  Компоненты из библиотек

//* секция Наши компоненты

//* endof  Наши компоненты

//* секция Стили компонента

//* endof  Стили компонента

export type ComponentProps = {
  /** Вариант цветового дизайна */
  variant?: string;
  isLoading?: boolean;
  /** Дополнительные классы для стилизации */
  className?: string;
  /** Если true, то рендерится html <button />, иначе рендерится обычный <div /> */
  isButtonHtml?: boolean;
  /** Насколько сильно увеличивать или уменьшать кнопку при взаимодействии с ней */
  scalingMultiplier: number;
  href?: string;
  children: React.ReactNode;
  /** Другие свойства html-элемента */
  [key: string]: any;
};

/**
 * Анимированная ссылка
 * @returns TSX Компонент
 */
export default React.forwardRef(function Component(
  {
    variant = "default", // default, green, danger
    isLoading = false,
    className = "",
    isButtonHtml = false,
    scalingMultiplier = 1,
    href = "",
    children,
    ...otherProps
  }: ComponentProps,
  elRef
) {
  let buttonType = null;
  const MotionComponent = isButtonHtml ? motion.button : motion.div;
  if (isButtonHtml) {
    className = `btn ${className}`;
    buttonType = "button";
  }
  return (
    <Link href={href}>
      <MotionComponent
        // ref={elRef}
        type={buttonType}
        className={`style-${variant} ${className} ${isLoading && "loading"}`}
        whileHover={{ scale: 1 + 0.06 * scalingMultiplier }}
        whileFocus={{ scale: 1 + 0.06 * scalingMultiplier }}
        whileTap={{ scale: 1 - 0.06 * scalingMultiplier }}
        // ставлю длину анимации 0, т.к. благодаря моей глобальной установке на анимацию всего, всё все равно анимируется, причем приятно.
        transition={{ duration: 0 }}
        {...otherProps}
      >
        {children}
        <div className="loader lds-ring color-brand1">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </MotionComponent>
    </Link>
  );
});
