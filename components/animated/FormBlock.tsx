//* секция Библиотеки c функциями
import React from "react";
import { motion, useAnimation } from "framer-motion";
// import { useIsFirstRender } from "@uidotdev/usehooks";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты
export type ComponentProps = {
  /** Значение */
  value?: any;
  /** Насколько сильно увеличивать или уменьшать этот компонент формы при взаимодействии с ним */
  scalingMultiplier?: number;
  /** Выставляемый z-index для компонента при открытом меню или просто наведении */
  activeZIndex?: number;
  /** Выставляемый z-index для компонента по умолчанию (с закрытым меню) */
  inactiveZIndex?: number;
  /** Классы, применяемые к компоненту */
  className?: string;
  /** Стиль, применяемый к компоненту */
  style?: Object;
  /** Название html-элемента, который будет здесь использоваться для рендера */
  htmlElement?: string;
  /** Название html-элемента, который будет здесь использоваться для рендера */
  children: React.ReactNode;
  /** Другие свойства html-элемента */
  [key: string]: any;
};

// TODO кажись inactiveZIndex тут должен быть хотя бы таким, какой activeZIndex в FormBlock
export default React.forwardRef(function Component(
  {
    value,
    scalingMultiplier = 1,
    activeZIndex = 2,
    inactiveZIndex = 1,
    className,
    htmlElement = "label",
    style,
    children,
    ...otherProps
  }: ComponentProps,
  elRef
) {
  //* библиотеки и неизменяемые значения
  const MotionComponent = motion[htmlElement];
  const defaultState = { scale: 1, zIndex: inactiveZIndex, boxShadow: "0" };
  const activeState = {
    scale: 1 + 0.02 * scalingMultiplier,
    zIndex: activeZIndex,
    boxShadow: "0 7px 10px rgba(0, 0, 0, 0.15)",
  };
  const pressedState = { scale: 1 - 0.02 * scalingMultiplier };
  //* endof библиотеки и неизменяемые значения

  //* контекст
  //* endof контекст

  //* состояние
  const [mustStayActive, setStayActive] = React.useState(false);
  const animationControls = useAnimation();

  //* endof состояние

  //* секция вычисляемые переменные, изменение состояния

  //* endof вычисляемые переменные, изменение состояния

  //* эффекты
  React.useImperativeHandle(elRef, () => ({
    externalSetStayActive(isActive) {
      console.log("externalSetStayActive called", isActive);
      setStayActive(isActive);
    },
  }));

  React.useEffect(() => {
    if (mustStayActive) {
      animationControls.set(activeState);
    } else {
    }
  }, [mustStayActive]);

  //* endof эффекты

  //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ

  //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ

  //* обработчики

  //* endof обработчики

  return (
    <>
      {/* TODO перенести классы из scss в локальные классы. Возможно, styled-components подойдут. Мне нужен класс, свойства которого я могу задавать в реалтайме - вроде бы он это умеет (No class name bugs). */}
      <MotionComponent
        animate={animationControls}
        onHoverStart={() => animationControls.start(activeState)}
        onHoverEnd={() => animationControls.start(defaultState)}
        whileFocus={activeState}
        // whileTap={pressedState}
        transition={{ duration: 0 }}
        className={`form-block ${
          mustStayActive && "active"
        } scalingMultiplier-${scalingMultiplier} activeZIndex-${activeZIndex} inactiveZIndex-${inactiveZIndex} ${className}`}
        style={style}
        {...otherProps}
      >
        {children}
      </MotionComponent>
    </>
  );
});
