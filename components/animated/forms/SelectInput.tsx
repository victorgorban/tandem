"use client";
//* секция Библиотеки c функциями
import React from "react";
import AsyncSelect from "react-select/async";
import { motion, useAnimation } from "framer-motion";

//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export type ComponentProps = {
  /** Значение */
  value?: any;
  /** Колбек для открытия или закрытия меню */
  onMenuOpenOrClosed?: (isOpen: boolean) => void;
  /** Насколько сильно увеличивать или уменьшать этот компонент формы при взаимодействии с ним */
  scalingMultiplier?: number;
  /** Выставляемый z-index для компонента при открытом меню или просто наведении */
  activeZIndex?: number;
  /** Выставляемый z-index для компонента по умолчанию (с закрытым меню) */
  inactiveZIndex?: number;
  /** Классы, применяемые к врапперу */
  wrapperClassName?: string;
  /** Классы, применяемые к компоненту react-select */
  className?: string;
  /** Стиль, применяемый к врапперу */
  wrapperStyle?: Object;
  /** Стиль, применяемый к компоненту react-select */
  style?: Object;
  /** Другие свойства html-элемента */
  [key: string]: any;
};

// TODO кажись inactiveZIndex тут должен быть хотя бы таким, какой activeZIndex в FormBlock
export default React.forwardRef(function Component(
  {
    value,
    scalingMultiplier = 1,
    activeZIndex = 2,
    inactiveZIndex = 1,
    wrapperClassName,
    className,
    wrapperStyle,
    style,
    onMenuOpenOrClosed,
    ...otherProps
  }: ComponentProps,
  elRef
) {
  //* библиотеки и неизменяемые значения
  const MotionComponent = motion["div"];

  const defaultState = { scale: 1, zIndex: inactiveZIndex };
  const activeState = {
    scale: 1 + 0.02 * scalingMultiplier,
    zIndex: activeZIndex,
  };
  const pressedState = { scale: 1 - 0.02 * scalingMultiplier };
  //* endof библиотеки и неизменяемые значения

  //* секция глобальное состояние из context
  //* endof глобальное состояние из context

  //* секция состояние
  const [mustStayActive, setStayActive] = React.useState(false);
  const animationControls = useAnimation();

  //* endof состояние

  //* секция вычисляемые переменные, изменение состояния

  //* endof вычисляемые переменные, изменение состояния

  //* секция эффекты
  React.useImperativeHandle(elRef, () => ({
    externalSetStayActive(isActive) {
      console.log("externalSetStayActive called", isActive);
      setStayActive(isActive);
    },
  }));

  React.useEffect(() => {
    if (mustStayActive) {
      animationControls.set(activeState);
    } else {
    }
  }, [mustStayActive]);
  //* endof эффекты

  //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ

  //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ

  //* секция обработчики
  function handleMenuOpen() {
    setStayActive(true);
    onMenuOpenOrClosed?.(true);
  }

  function handleMenuClose() {
    setStayActive(false);
    onMenuOpenOrClosed?.(false);
  }
  //* endof обработчики

  return (
    // Я не понимаю, почему то, что работало в FormBlock, здесь не работает нормально? Враппер такой же плюс-минус...
    <MotionComponent
      animate={animationControls}
      onHoverStart={() => animationControls.start(activeState)}
      onHoverEnd={() => animationControls.start(defaultState)}
      whileFocus={activeState}
      // whileTap={pressedState}
      transition={{ duration: 0 }}
      className={`w-100 form-block ${
        mustStayActive && "active"
      } scalingMultiplier-${scalingMultiplier} activeZIndex-${activeZIndex} inactiveZIndex-${inactiveZIndex} ${wrapperClassName}`}
      style={wrapperStyle}
      {...otherProps}
    >
      <AsyncSelect
        noOptionsMessage={() => "Ничего не найдено..."}
        className={`w-100 ${className}`}
        classNamePrefix="react-select"
        styles={{
          menu: (base) => ({ ...base, zIndex: 999 }),
          // menu: base => ({ ...base, zIndex: 9999 }),
        }}
        // menuPortalTarget={document.body}
        // document.body в модалке плохо работает. Можно не указывать, и поставить menuPosition="fixed", так работает
        menuShouldScrollIntoView={true}
        // menuPlacement="auto"
        // menuPosition={"fixed"}
        placeholder="Выбрать..."
        onMenuOpen={handleMenuOpen}
        onMenuClose={handleMenuClose}
        value={value}
        {...otherProps}
      />
    </MotionComponent>
  );
});
